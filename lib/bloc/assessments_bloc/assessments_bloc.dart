import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:newborn_care/models/child_model.dart';
import 'package:newborn_care/models/stage_1.dart';
import 'package:newborn_care/models/stage_2.dart';
import 'package:newborn_care/models/stage_4.dart';
import 'package:newborn_care/models/stage_5.dart';
import 'package:newborn_care/repository/assessments_repository.dart';
import 'package:newborn_care/repository/hive_storage_repository.dart';
import 'package:newborn_care/repository/notification_repository.dart';
part 'assessments_event.dart';
part 'assessments_state.dart';

class AssessmentsBloc extends Bloc<AssessmentsEvent, AssessmentsState> {
  final AssessmentsRepository _assessmentsRepository;
  final HiveStorageRepository hiveStorageRepository;
  final NotificationRepository notificationRepository;
  final ChildModel childModel;

  AssessmentsBloc(
      this.notificationRepository,
      this._assessmentsRepository,
      this.childModel,
      this.hiveStorageRepository,
      ) : super(AssessmentsInitial(childModel)) {
    on<AssessmentsEventFetchData>(_onFetchData);
    on<AssessmentsEventCompleteStage1>(_onCompleteStage1);
    on<AssessmentsEventCompleteStage2>(_onCompleteStage2);
    on<AssessmentsEventCompleteStage3>(_onCompleteStage3);
    on<AssessmentsEventCompleteStage4>(_onCompleteStage4);
    on<AssessmentsEventCompleteStage5>(_onCompleteStage5);
    on<DischargeButtonClick>(_onDischarge);
  }

  Future<void> _onFetchData(
      AssessmentsEventFetchData event,
      Emitter<AssessmentsState> emit,
      ) async {
    emit(AssessmentsLoading(childModel));
    try {
      childModel.assessmentsList =
      await _assessmentsRepository.fetchAssessments(childModel.key);
      childModel.assessmentsList =
          _assessmentsRepository.addNextAssessment(childModel);
      hiveStorageRepository.updateChild(childModel.key, childModel);
      emit(AssessmentsInitial(childModel));
    } catch (e) {
      emit(AssessmentsError(e.toString()));
      emit(AssessmentsInitial(childModel));
    }
  }

  Future<void> _onCompleteStage1(
      AssessmentsEventCompleteStage1 event,
      Emitter<AssessmentsState> emit,
      ) async {
    try {
      _assessmentsRepository
          .validatePhase1Assessments(childModel.assessmentsList[0] as Stage1);
      await notificationRepository.removeScheduledNotification(childModel.key);
      await _assessmentsRepository.registerStageDetails(
          childModel.assessmentsList[0], childModel.key);

      childModel.assessmentsList =
          _assessmentsRepository.addNextAssessment(childModel);
      await _assessmentsRepository.updateTrackedEntityInstance(
          childModel,
          childModel.key,
          (childModel.assessmentsList[0] as Stage1).ecebWardName);
      hiveStorageRepository.updateChild(childModel.key, childModel);
      emit(AssessmentsAdded(childModel));
    } catch (e) {
      emit(AssessmentsError(e.toString()));
      emit(AssessmentsInitial(childModel));
    }
  }

  Future<void> _onCompleteStage2(
      AssessmentsEventCompleteStage2 event,
      Emitter<AssessmentsState> emit,
      ) async {
    try {
      _assessmentsRepository.validatePhase2Assessments(
          childModel.assessmentsList[1] as Stage2, childModel.birthTime);

      await notificationRepository.removeScheduledNotification(childModel.key);
      await _assessmentsRepository.registerStageDetails(
          childModel.assessmentsList[1], childModel.key);

      childModel.classification = _assessmentsRepository
          .classifyHealthAfterStage2(childModel.assessmentsList[1] as Stage2);
      _assessmentsRepository.changeColorBasedOnClassification(childModel);

      childModel.assessmentsList =
          _assessmentsRepository.addNextAssessment(childModel);

      await _assessmentsRepository.updateTrackedEntityInstance(
          childModel,
          childModel.key,
          (childModel.assessmentsList[1] as Stage2).ecebWardName);

      hiveStorageRepository.updateChild(childModel.key, childModel);
      emit(AssessmentsAdded(childModel));
    } catch (e) {
      emit(AssessmentsError(e.toString()));
      emit(AssessmentsInitial(childModel));
    }
  }

  Future<void> _onCompleteStage3(
      AssessmentsEventCompleteStage3 event,
      Emitter<AssessmentsState> emit,
      ) async {
    try {
      _assessmentsRepository
          .validatePhase3Assessments(childModel.assessmentsList[event.index]);

      await notificationRepository.removeScheduledNotification(childModel.key);
      await _assessmentsRepository.registerStageDetails(
          childModel.assessmentsList[event.index], childModel.key);

      childModel.assessmentsList =
          _assessmentsRepository.addNextAssessment(childModel);

      hiveStorageRepository.updateChild(childModel.key, childModel);
      emit(AssessmentsAdded(childModel));
    } catch (e) {
      emit(AssessmentsError(e.toString()));
      emit(AssessmentsInitial(childModel));
    }
  }

  Future<void> _onCompleteStage4(
      AssessmentsEventCompleteStage4 event,
      Emitter<AssessmentsState> emit,
      ) async {
    try {
      _assessmentsRepository.validatePhase4Assessments(
          childModel.assessmentsList[event.index] as Stage4);

      await notificationRepository.removeScheduledNotification(childModel.key);
      await _assessmentsRepository.registerStageDetails(
          childModel.assessmentsList[event.index], childModel.key);

      childModel.classification =
          _assessmentsRepository.classifyHealthAfterStage4(
              childModel.assessmentsList[event.index] as Stage4);
      _assessmentsRepository.changeColorBasedOnClassification(childModel);

      childModel.assessmentsList =
          _assessmentsRepository.addNextAssessment(childModel);

      await _assessmentsRepository.updateTrackedEntityInstance(
          childModel,
          childModel.key,
          (childModel.assessmentsList[1] as Stage2).ecebWardName);

      hiveStorageRepository.updateChild(childModel.key, childModel);
      emit(AssessmentsAdded(childModel));
    } catch (e) {
      emit(AssessmentsError(e.toString()));
      emit(AssessmentsInitial(childModel));
    }
  }

  Future<void> _onCompleteStage5(
      AssessmentsEventCompleteStage5 event,
      Emitter<AssessmentsState> emit,
      ) async {
    try {
      _assessmentsRepository.validatePhase5Assessments(
          childModel.assessmentsList.last as Stage5);
      childModel.isCompleted = true;
      await _assessmentsRepository.registerStageDetails(
          childModel.assessmentsList.last, childModel.key);
      await _assessmentsRepository.updateEnrollmentStatus(childModel.key);
      hiveStorageRepository.updateChild(childModel.key, childModel);
      emit(AssessmentsAdded(childModel));
    } catch (e) {
      emit(AssessmentsError(e.toString()));
      emit(AssessmentsInitial(childModel));
    }
  }

  Future<void> _onDischarge(
      DischargeButtonClick event,
      Emitter<AssessmentsState> emit,
      ) async {
    childModel.assessmentsList =
        _assessmentsRepository.removeLastUncompletedAssessment(childModel);
    await notificationRepository.removeScheduledNotification(childModel.key);

    childModel.assessmentsList =
        _assessmentsRepository.addDischargeAssessments(childModel);
    hiveStorageRepository.updateChild(childModel.key, childModel);
    emit(AssessmentsInitial(childModel));
  }
}