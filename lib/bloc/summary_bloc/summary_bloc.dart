import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:newborn_care/repository/hive_storage_repository.dart';
import 'package:newborn_care/repository/summary_repository.dart';

part 'summary_event.dart';
part 'summary_state.dart';

class SummaryBloc extends Bloc<SummaryEvent, SummaryState> {
  final SummaryRepository summaryRepository;
  final HiveStorageRepository hiveStorageRepository;

  SummaryBloc(
      this.summaryRepository,
      this.hiveStorageRepository,
      ) : super(SummaryInitial(0, 0, 0)) {
    on<FetchSummaryOf24Hours>(_onFetchSummary);
  }

  Future<void> _onFetchSummary(
      FetchSummaryOf24Hours event,
      Emitter<SummaryState> emit,
      ) async {
    final res = await summaryRepository.fetchSummaryOf24Hours();
    emit(SummaryInitial(res[0], res[1], res[2]));
  }
}