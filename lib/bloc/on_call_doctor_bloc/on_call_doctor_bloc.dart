import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:newborn_care/models/on_call_doctor_model.dart';
import 'package:newborn_care/repository/hive_storage_repository.dart';
import 'package:newborn_care/repository/on_call_doctor_repository.dart';

part 'on_call_doctor_event.dart';
part 'on_call_doctor_state.dart';

class OnCallDoctorBloc extends Bloc<OnCallDoctorEvent, OnCallDoctorState> {
  final OnCallDoctorRepository onCallDoctorRepository;
  final HiveStorageRepository hiveStorageRepository;

  OnCallDoctorBloc(
      this.onCallDoctorRepository,
      this.hiveStorageRepository,
      ) : super(OnCallDoctorLoading()) {
    on<FetchOnCallDoctors>(_onFetchOnCallDoctors);
  }

  Future<void> _onFetchOnCallDoctors(
      FetchOnCallDoctors event,
      Emitter<OnCallDoctorState> emit,
      ) async {
    emit(OnCallDoctorLoading());

    try {
      final list = await onCallDoctorRepository.getListOfOnCallDoctors();
      emit(OnCallDoctorLoaded(list));
    } catch (e) {
      // Fallback to local storage if remote fetch fails
      final result = hiveStorageRepository.getOnCallDoctors();
      final separatedDoctors = onCallDoctorRepository.seperateDoctorsOnCall(result);
      emit(OnCallDoctorLoaded(separatedDoctors));
    }
  }
}