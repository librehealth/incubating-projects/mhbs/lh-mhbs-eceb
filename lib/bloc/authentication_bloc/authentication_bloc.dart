import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:newborn_care/models/profile.dart';
import 'package:newborn_care/repository/hive_storage_repository.dart';
import 'package:newborn_care/repository/authentication_repository.dart';
import 'package:newborn_care/repository/program_rule_repository.dart';
part 'authentication_event.dart';
part 'authentication_state.dart';

class AuthenticationBloc
    extends Bloc<AuthenticationEvent, AuthenticationState> {
  final AuthenticationRepository authenticationRepository;
  final ProgramRuleRepository programRuleRepository;
  final HiveStorageRepository hiveStorageRepository;

  AuthenticationBloc(
      this.authenticationRepository,
      this.hiveStorageRepository,
      this.programRuleRepository,
      ) : super(AuthenticationInitial()) {
    on<AuthenticationLoginEvent>(_onAuthenticationLogin);
  }

  Future<void> _onAuthenticationLogin(
      AuthenticationLoginEvent event,
      Emitter<AuthenticationState> emit,
      ) async {
    emit(AuthenticationLoading());

    try {
      final profile = await authenticationRepository.loginUser(
        event.username,
        event.password,
        event.server,
        event.orgUnit,
      );

      hiveStorageRepository.storeProfile(profile);
      emit(AuthenticationLoaded());

      // Since updateProgramRules is void, we call it without await
      programRuleRepository.updateProgramRules();
    } catch (e) {
      emit(AuthenticationError(e.toString()));
    }
  }
}