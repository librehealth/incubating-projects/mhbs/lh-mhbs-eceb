import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:newborn_care/repository/hive_storage_repository.dart';

part 'settings_event.dart';
part 'settings_state.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  final HiveStorageRepository hiveStorageRepository;

  SettingsBloc(this.hiveStorageRepository)
      : super(SettingsState(
    hiveStorageRepository.getThemeData(),
    hiveStorageRepository.getNotificationSoundEnabled(),
    hiveStorageRepository.getNotificationEnabled(),
  )) {
    on<SettingsChanged>(_onSettingsChanged);
  }

  void _onSettingsChanged(
      SettingsChanged event,
      Emitter<SettingsState> emit,
      ) {
    // Store new settings in repository
    hiveStorageRepository.storeThemeData(event.isDarkModeEnabled);
    hiveStorageRepository.storeNotificationSoundEnabled(event.isNotificationSoundEnabled);
    hiveStorageRepository.storeNotificationEnabled(event.isNotificationEnabled);

    // Emit new state with updated settings
    emit(SettingsState(
      event.isDarkModeEnabled,
      event.isNotificationSoundEnabled,
      event.isNotificationEnabled,
    ));
  }
}