import 'package:bloc/bloc.dart';
import 'package:newborn_care/models/notification_model.dart';
import 'package:newborn_care/repository/notification_screen_repository.dart';
part 'notification_event.dart';
part 'notification_state.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  final NotificationScreenRepository notificationScreenRepository;

  NotificationBloc(this.notificationScreenRepository)
      : super(NotificationLoading()) {
    on<FetchNofiticationOfBabies>(_onFetchNotifications);
  }

  void _onFetchNotifications(
      FetchNofiticationOfBabies event,
      Emitter<NotificationState> emit,
      ) {
    emit(NotificationLoading());

    final monitoringAlerts = notificationScreenRepository.fetchMonitoringAlerts();
    final riskAssessments = notificationScreenRepository.fetchRiskAssessmentsList();

    emit(NotificationLoaded(monitoringAlerts, riskAssessments));
  }
}